#!/usr/bin/env python
import pika
import random

f = open('auth.txt','r')
q = f.read()
r = q.split('\n')

user = r[0]
password = r[1]
ipaddr = r[2]
port = r[3]

credentials = pika.PlainCredentials(user, password)
parameters = pika.ConnectionParameters(ipaddr,
                                       int(port),
                                       '/',
                                       credentials)

connection = pika.BlockingConnection(parameters)
channel = connection.channel()
channel1 = connection.channel()

channel.queue_declare(queue='q2')
channel1.queue_declare(queue='q1')

def callback(ch, method, properties, body):

    response = [
        'Really?',
        'That\'s cool.',
        'The world is a vampire',
        'How\'s that?',
        'I like cats',
        'Dark Shadows is cool'
    ]


    print(" [x] Received %r" % body)

    myresp = response[random.randint(0,len(response)-1)]

    sombool = channel1.basic_publish(exchange='',
                                    routing_key='q1',
                                    body=myresp)
    ch.basic_ack(delivery_tag = method.delivery_tag)
    print(" [x] Sent {}".format(myresp))

channel.basic_consume(callback,
                      queue='q2')

#print(' [*] Waiting for messages. To exit press CTRL+C')
channel.start_consuming()



