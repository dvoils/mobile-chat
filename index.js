var app = require('express')()
var http = require('http').Server(app)
var io = require('socket.io')(http)

var async = require('async')
var fs = require('fs')
            
//var app = express();

var path = require('path');
var amqp = require('amqplib/callback_api');

app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname + '/index.html'))
})

var data = fs.readFileSync('auth.txt')
abc = data.toString()
a = abc.split('\n')
console.log("Synchronous read: " + a[0])

var user = a[0]
var password = a[1]
var ipaddr = a[2]
var port = a[3]

var create_receive_channel = function() {
    amqp.connect('amqp://' + user + ':' + password + '@' + ipaddr, + ':' + port, function(err, conn) {
        app.recQ = conn.createChannel(function(err, ch) {
            var q1 = 'q1'
            ch.assertQueue(q1, {durable: false})
            console.log("starting q1")
        });
    });
};

var create_send_channel = function() {
    amqp.connect('amqp://' + user + ':' + password + '@' + ipaddr, + ':' + port, function(err, conn) {
        app.sendQ = conn.createChannel(function(err, ch) {
            console.log("starting q2")
            var q = 'q2'
            ch.assertQueue(q, {durable: false})
        })
    })
}

var receive_consume = function(callback) {
    app.recQ.consume('q1', function(msg) {
        var outmsg = msg.content.toString()
        app.recQ.ack(msg)
        callback(outmsg)
    })
}

create_receive_channel()
create_send_channel()

var port = process.env.PORT || 8080;

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
})

io.on('connection', function(socket){
  socket.on('chat message', function(msg){
    app.sendQ.sendToQueue('q2', new Buffer.from(msg))
    
    receive_consume(function(outmsg) {
      console.log('recieved', outmsg)
      io.emit('chat message', outmsg)
    })
    
    
  })
})

http.listen(port, function(){
  console.log('listening on *:' + port)
});
